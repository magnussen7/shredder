#!/usr/bin/python3
# coding: utf-8
import argparse
import os
import glob
import math
import string
import random
from PIL import Image

def shredder(source):
    try:
        with Image.open(source) as image:
            alphabet = string.ascii_letters + string.digits

            width = image.size[0]
            height = image.size[1]

            divisor = []

            for i in range(1, int(math.sqrt(width) + 1)):
                if width % i == 0:
                    if i * i != width:
                        divisor.append(width / i)
                    #END IF
                #END IF
            #END FOR

            widthBand = int(divisor[-1])
            nbBand = int(width / widthBand)

            if not os.path.exists("./shred"):
                os.makedirs("./shred")
            #END IF

            for i in range(nbBand):

                filename = ""

                for j in range (8):
                    filename += alphabet[random.randint(0, len(alphabet)-1)]
                #END FOR

                canevas = (i * widthBand, 0, widthBand + (i * widthBand), height)
                shred = image.crop(canevas)
                shred.convert('1')
                shred.save("./shred/" + filename + ".png")
            #END FOR
        #END WITH
    except Exception as e:
        print("An error as occured.")
##END DEF

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Shred a file')
    parser.add_argument('source', type=argparse.FileType('rb'), help='Input Image')
    args = parser.parse_args()

    shredder(args.source)
